#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess
from subprocess import Popen, PIPE
import time
import zipfile
import os

if(os.path.isfile('/home/kan/My_files/BigData/dataset.zip')==True): #проверка существования архива в директории с проектом
	print("archieve exists")
else:
#скачиваем данные
	print("download started")
	proc = Popen(
	    "python /home/kan/My_files/BigData/download_script.py",
	    shell=True,
	    stdout=PIPE, stderr=PIPE
	)
	proc.wait()    # дождаться выполнения
	res = proc.communicate()  # получить tuple('stdout', 'stderr')
	if proc.returncode:
    	    print res[1]
	print 'download is completed', res[0]

#распаковка данных из zip
	proc = Popen(
	    "python /home/kan/My_files/BigData/zip_extractor.py",
	    shell=True,
	    stdout=PIPE, stderr=PIPE
	)
	proc.wait()    # дождаться выполнения
	res = proc.communicate()  # получить tuple('stdout', 'stderr')
	if proc.returncode:
	    print res[1]
	print 'extraction is completed', res[0]

#распаковка текстовых файлов из bz2
	subprocess.call("/home/kan/My_files/BigData/sh_bz2.sh", shell=True)
	print 'files are ready for MapReducing'

t1 = time.time()
#запуск hadoop и расчета
subprocess.call("/home/kan/My_files/BigData/hdfs.sh", shell=True)
print("--- %s seconds ---" % (time.time() - t1)) #вывод времени выполнения MapReduce

#тестирование
answer = raw_input("Do you need a test? ")
if answer == "yes":
	proc = Popen(
	    "python /home/kan/My_files/BigData/test.py",
	    shell=True,
	    stdout=PIPE, stderr=PIPE
	)
	proc.wait()    # дождаться выполнения
	res = proc.communicate()  # получить tuple('stdout', 'stderr')
	if proc.returncode:
	    print res[1]
	print res[0]
else:
	exit()

