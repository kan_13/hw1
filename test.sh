#!/bin/bash

#копирование файлов из локальной системы в hdfs
/opt/hadoop-2.9.1/bin/hdfs dfs -put /home/kan/My_files/BigData/check.txt input
echo '****************************checkFile is copied****************************'

#запуск расчета
/opt/hadoop-2.9.1/bin/hadoop jar /opt/hadoop-2.9.1/share/hadoop/tools/lib/hadoop-streaming-2.9.1.jar -file /home/kan/My_files/BigData/mapper.py -mapper "python /home/kan/My_files/BigData/mapper.py" -file /home/kan/My_files/BigData/reducer.py -reducer "python /home/kan/My_files/BigData/reducer.py" -input input/check.txt -output /home/kan/My_files/BigData/output_test.csv

#копирование результатов в локальную систему
/opt/hadoop-2.9.1/bin/hdfs dfs -copyToLocal /home/kan/My_files/BigData/output_test.csv /home/kan/My_files/BigData/

#удаление результатов из hdfs
/opt/hadoop-2.9.1/bin/hdfs dfs -rm -R /home/kan/My_files/BigData/output_test.csv/

#удаление папки со входными данными
#/opt/hadoop-2.9.1/bin/hdfs dfs -rm /user/kan/input/check.txt

#остановка работы нод
#/opt/hadoop-2.9.1/sbin/stop-dfs.sh
