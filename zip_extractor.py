#!/usr/bin/env python
# -*- coding: utf-8 -*-

import zipfile
import os

#распаковка указанных файлов из архива zip
z = zipfile.ZipFile('dataset.zip', 'r')
z.extract('ipinyou.contest.dataset/city.en.txt')
z.extract('ipinyou.contest.dataset/training3rd/imp.20131019.txt.bz2')
z.extract('ipinyou.contest.dataset/training3rd/imp.20131020.txt.bz2')
z.extract('ipinyou.contest.dataset/training3rd/imp.20131021.txt.bz2')
z.extract('ipinyou.contest.dataset/training3rd/imp.20131022.txt.bz2')
z.extract('ipinyou.contest.dataset/training3rd/imp.20131023.txt.bz2')
z.extract('ipinyou.contest.dataset/training3rd/imp.20131024.txt.bz2')
z.extract('ipinyou.contest.dataset/training3rd/imp.20131025.txt.bz2')
z.extract('ipinyou.contest.dataset/training3rd/imp.20131026.txt.bz2')
z.extract('ipinyou.contest.dataset/training3rd/imp.20131027.txt.bz2')

#переименование директорий в более удобные
os.rename("ipinyou.contest.dataset", "my_dataset")
os.rename('my_dataset/training3rd', 'my_dataset/input')
