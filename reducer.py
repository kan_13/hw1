#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
from itertools import groupby
from operator import itemgetter
import sys

#функция для считывания данных с маппера
def read_mapper_output(file, separator='\t'):
    for line in file:
        yield line.rstrip().split(separator, 1)
 
def main(separator='\t'):
    data = read_mapper_output(sys.stdin, separator=separator)

#группировка полученных данных
    for current_word, group in groupby(data, itemgetter(0)):
        try:
            total_count = sum(int(count) for current_word, count in group)
            print ("%s%s%d" % (current_word, separator, total_count)) #вывод результатов
        except ValueError:
            pass
 
if __name__ == "__main__":
    main()
