#!/bin/bash

#запуск нод Hadoop
#/opt/hadoop-2.9.1/bin/hdfs namenode -format
#/opt/hadoop-2.9.1/sbin/start-dfs.sh

#создание необходимых директорий
#/opt/hadoop-2.9.1/bin/hdfs dfs -mkdir /user
#/opt/hadoop-2.9.1/bin/hdfs dfs -mkdir /user/kan
#/opt/hadoop-2.9.1/bin/hdfs dfs -mkdir /user/kan/input

#копирование файлов из локальной системы в hdfs
/opt/hadoop-2.9.1/bin/hdfs dfs -put /home/kan/My_files/BigData/my_dataset/input input
echo '****************************input is copied****************************'

#запуск расчета
/opt/hadoop-2.9.1/bin/hadoop jar /opt/hadoop-2.9.1/share/hadoop/tools/lib/hadoop-streaming-2.9.1.jar -file /home/kan/My_files/BigData/mapper.py -mapper "python /home/kan/My_files/BigData/mapper.py" -file /home/kan/My_files/BigData/reducer.py -reducer "python /home/kan/My_files/BigData/reducer.py" -input input/input/imp.20131027.txt -output /home/kan/My_files/BigData/output.csv

#копирование результатов в локальную систему
/opt/hadoop-2.9.1/bin/hdfs dfs -copyToLocal /home/kan/My_files/BigData/output.csv /home/kan/My_files/BigData/

#удаление результатов из hdfs
/opt/hadoop-2.9.1/bin/hdfs dfs -rm -R /home/kan/My_files/BigData/output.csv/

#удаление папки со входными данными
#/opt/hadoop-2.9.1/bin/hdfs dfs -rm -R /user/kan/input/input

#остановка работы нод
#/opt/hadoop-2.9.1/sbin/stop-dfs.sh

